/**
 * Created by theotheu on 25-07-16.
 */
var fs = require('fs'),
    path = require('path'),
    XmlStream = require('xml-stream'),
    mongoose = require('mongoose'),
    env, config, database;


/**
 * Load configuration
 */
env = process.env.NODE_ENV || 'development'; // You can use the environment-variable 'NODE_ENV' to determine the environment being used. Fall back to 'development' when this is unset.
config = require('../server/config/config.js')[env]; // Load configuration file;
database = config.db;

mongoose.connect(database);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

// Create a file stream and pass it to XmlStream
function setup(encoding) {
    var stream = fs.createReadStream(path.join(__dirname, 'bredius.xml'));
    console.log('>>>>> stream');

    var xml = new XmlStream(stream, encoding);
    var dbName;
    console.log('>>>>> xml');

    var schemaName;
    schemaName = mongoose.Schema({
        "database": {
            "text": {"type": String, "required": true},
            "name": {"type": String, "required": true},
            "path": {"type": String, "required": true}
        },
        "sourceApp": {
            "text": {"type": String, "required": true},
            "name": {"type": String, 'required': true},
            "version": {"type": String, "required": true}
        },
        "recNumber": {"type": String, "required": true},
        "refType": {
            "text": {"type": String},
            "name": {"type": String}
        },

        "contributors": {
            "authors": [],
            "secondaryAuthors": [],
            "tertiaryAuthors": []
        },
        "titles": {
            "title": {"type": String},
            "tertiaryTitle": {"type": String}
        },
        "pages": {
            "name": {type: String},
            "start": {type: String},
            "end": {type: String},
        },
        "number": {"type": String},
        "reprintStatus": {
            "status": {"type": String}
        },
        "keywords": [],
        "dates": [],
        "publisher": {"type": String},
        "urls": {},
        "availability": {"type": String},
        "isbn": {"type": String},
        "periodical": {"type": String},
        "volume": {"type": String},
        "pubLocation": {"type": String},
        "notes": {"type": String},
        "custom1": {"type": String},
        "custom2": {"type": String},
        "creationDate": {type: Date, default: Date.now},
        "modificationDate": {type: Date, default: Date.now},
        "createdBy": {type: String, default: "admin"},
        "modifiedBy": {type: String, default: "admin"}
    });

    var BookModel = mongoose.model('Book', schemaName);

    var count = 0;
    var labels = {};
    labels['authors'] = 'authors';
    labels['author'] = 'author';
    labels['title'] = 'title';
    labels['source-app'] = 'sourceApp';
    labels['rec-number'] = 'recNumber';
    labels['ref-type'] = 'refType';
    labels['reprint-status'] = 'reprintStatus';
    labels['tertiary-authors'] = 'tertiaryAuthors';
    labels['secondary-authors'] = 'secondaryAuthors';
    labels['tertiary-title'] = 'tertiaryTitle';
    labels['pub-location'] = 'pubLocation';

    xml.preserve('record');
    xml.on('endElement: record', function (item) {
        var dbBook, book = {};
        var obj = item['$children'];

        Object.keys(obj).forEach(function (key) {
            var name = obj[key].$name;
            if (labels[name] !== undefined) {
                dbName = labels[name];
            } else {
                dbName = name;
            }
            var i, j, n, c, cc, dbName2;
            // titles
            if (name === 'titles') {
                book[dbName] = {};
                n = obj[key].$children;
                for (i = 0; i < n.length; i++) {
                    if (n[i].$name.match(/title/i)) {
                        dbName2 = labels[n[i].$name];
                        book[dbName][dbName2] = n[i].style.$text;
                    }
                }
            } else if (name === 'number') {
                n = obj[key].$children;
                book[dbName] = n[0].$text;
            } else if (name === 'author') {
                n = obj[key].$children;
                book[dbName] = n[0].$text;
            } else if (name === 'database') {
                n = obj[key];
                book[dbName] = {text: n.$text, name: n.$.name, path: n.$.path};
            } else if (name === 'source-app') {
                n = obj[key];
                book[dbName] = {text: n.$text, name: n.$.name, version: n.$.version};
            } else if (name === 'ref-type') {
                n = obj[key];
                book[dbName] = {text: n.$text, name: n.$.name};
            } else if (name === 'reprint-status') {
                n = obj[key];
                book[dbName] = {};
                book[dbName].status = n.$.status;
            } else if (name === 'publisher') {
                n = obj[key].$children;
                book[dbName] = n[0].$text;
            } else if (name === 'custom1') {
                n = obj[key].$children;
                book[dbName] = n[0].$text;
            } else if (name === 'custom2') {
                n = obj[key].$children;
                book[dbName] = n[0].$text;
            } else if (name === 'contributors') {
                c = obj[key].$children;
                if (c !== undefined) {
                    book[dbName] = {};
                    for (i = 0; i < c.length; i++) {
                        dbName2 = labels[c[i].$name];
                        book[dbName][dbName2] = [];
                        cc = c[i].$children;
                        for (j = 0; j < cc.length; j++) {
                            book[dbName][dbName2].push(cc[j].$children[0].$text);
                        }
                    }
                }
            } else if (name === 'periodical') {
                if (obj && obj[key] && obj[key].hasOwnProperty('$children')) {
                    n = obj[key].$children;
                    if (n[0] && n[0].hasOwnProperty('style')) {
                        book[dbName] = n[0].style.$text;
                    }
                }
            } else if (name === 'isbn') {
                n = obj[key].$children[0];
                book[dbName] = n.$text;
            } else if (name === 'volume') {
                n = obj[key].$children[0];
                book[dbName] = n.$text;
            } else if (name === 'pages') {
                n = obj[key];
                book[dbName] = {
                    name: n.$text,
                    start: n.$.start,
                    end: n.$.end
                };
            } else if (name === 'pub-location') {
                n = obj[key].$children[0];
                book[dbName] = n.$text;
            } else if (name === 'notes') {
                n = obj[key].$children[0];
                book[dbName] = n.$text;
            } else if (name === 'availability') {
                n = obj[key].$children[0];
                book[dbName] = n.$text;
            } else if (name === 'keywords') {
                n = obj[key].$children;
                book[dbName] = [];
                for (i = 0; i < n.length; i++) {
                    book[dbName].push(n[i].$children[0].$text);
                }
            } else if (name === 'dates') {
                n = obj[key].$children;
                book[dbName] = [];
                for (i = 0; i < n.length; i++) {
                    book[dbName].push({date: n[i].$, year: n[i].$text});
                }

                // }
            } else if (name === 'urls') {
                n = obj[key].$children;
                book[dbName] = {};
                for (i = 0; i < n.length; i++) {
                    cc = n[i].$children;
                    dbName2 = n[i].$name;
                    book[dbName][dbName2] = [];
                    for (j = 0; j < cc.length; j++) {
                        book[dbName][dbName2].push(cc[j].$text);
                    }
                }
            } else if (obj[key].$text !== undefined) {
                var val = obj[key].$text;
                book[dbName] = val;
            } else {
                console.log('TODO', count, obj[key].$name, obj[key]);
                process.exit(1);
            }
        });

        // console.log('\n------\n', JSON.stringify(book), '\n------\n');

        dbBook = new BookModel(book);
        dbBook.save(function (err, doc) {
            console.log('count', count);
            if (!err) {
            } else {
                console.log(book);
                console.log(count, err);
                process.exit(1);

            }
            count++;

        });


    });

    // xml.on('end', function (err) {
    //     console.log('Done!');
    //     process.exit(1);
    // });
    //
    // xml.collect('record');
    // xml.on('endElement: records', function (item) {
    //     // console.log(item);
    //     console.log('********');
    //     process.exit(1);
    // });


}

db.once('open', function () {
    console.log("we're connected!");
    var xml = setup('utf8');       // Parse as UTF-8

});
