#!/usr/bin/env bash

BASE=bredius/client/dist/images/coins
FOLDER=1

for img in $BASE/large/$FOLDER/*.jpg; do

        FILENAME=$(basename $img)
        CMD="convert -fuzz 75% -trim -resize 300x300 $img $BASE/small/$FOLDER/$FILENAME"
        echo $CMD
        `$CMD`
done