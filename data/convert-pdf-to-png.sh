#!/bin/bash
# @see https://developer.apple.com/legacy/library/documentation/Darwin/Reference/ManPages/man1/sips.1.html
#
# !!! MAC OS X only !!!
#

rm -fr ../client/app/images/icons/large
rm -fr ../client/app/images/icons/small

mkdir -p ../client/app/images/icons/large
mkdir -p ../client/app/images/icons/small
mkdir -p ../client/dist/images/icons/large
mkdir -p ../client/dist/images/icons/small
cd pdfs

for i in *.pdf; do

    FILENAME="${i/\.pdf/.png}"

    echo
    echo "From $i to $FILENAME"

    sips -s format png $i --out ../../client/app/images/icons/large/$FILENAME;

    cp -f ../../client/app/images/icons/large/$FILENAME ../../client/app/images/icons/small/$FILENAME

    sips -Z 300 ../../client/app/images/icons/small/$FILENAME

    cp -f ../../client/app/images/icons/large/$FILENAME ../../client/dist/images/icons/large/$FILENAME
    cp -f ../../client/app/images/icons/small/$FILENAME ../../client/dist/images/icons/small/$FILENAME

done
