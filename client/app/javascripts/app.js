/**
 * Created by theotheu on 26-07-16.
 */


(function () {
    window.brediusApp = {

        limit: 10, // show max results per page, suggestions
        offset: 0, // start at first page
        searchResult: {}, // result from last search
        isAutocompleteSearch: false,
        page: window.location.pathname.replace(".html", "").replace("/", "") || null, // must be the same name as the route on server side
        endpoint: "/api/",
        // endpoint: "http://node155.tezzt.nl:3000/api/" + brediusApp.page + "/search/",
        labels: {
            books: {
                titles: {title: 'title', tertiaryTitle: 'series'},
                contributor: {authors: 'author(s)', secondaryAuthors: 'editor(s)', tertiaryAuthors: 'series editor(s)'},
                dates: 'pub.date',
                keywords: 'notes',
                volume: 'editon',
                publisher: 'publisher',
                isbn: 'isbn/issn',
                custom1: 'custom1',
                custom2: 'custom2',
                custom3: 'custom3',
                numVols: 'numVols',
                misc1: 'misc1',
                misc2: 'misc2',
                misc3: 'misc3',
                misc4: 'misc4',
                recNumber: 'recNumber'
            },
            icons: {
                titles: {title: 'title/subject', tertiaryTitle: 'series'},
                contributor: {authors: 'artist', secondaryAuthors: 'editor(s)', tertiaryAuthors: 'series editor(s)'},
                dates: 'pub.date',
                keywords: 'notes',
                volume: 'editon',
                publisher: 'publisher',
                isbn: 'isbn/issn',
                custom1: 'custom1',
                custom2: 'custom2',
                custom3: 'custom3',
                numVols: 'numVols',
                misc1: 'Type of work',
                misc2: 'misc2',
                misc3: 'misc3',
                misc4: 'misc4',
                recNumber: 'recNumber'
            },
            coins: {
                titles: 'title',
                dates: 'pub.date',
                keywords: 'notes',
                volume: 'editon',
                publisher: 'publisher',
                custom1: 'custom1',
                custom2: 'custom2',
                custom3: 'custom3',
                numVols: 'numVols',
                misc1: 'Type of work',
                misc2: 'misc2',
                misc3: 'misc3',
                misc4: 'misc4',
                recNumber: 'recNumber'
            }
        },

        search: function () {
            var $q = $("#q"),
                q = $q.val(),
                url = brediusApp.endpoint + q;

            if (q === '') {
                return;
            }

            brediusApp.isAutocompleteSearch = $('.ui-autocomplete').css('display').toLowerCase() === 'block';

            $("body").css("cursor", "progress");

            console.log('url >>>>>', brediusApp.endpoint + q + "?limit=" + brediusApp.limit + "&offset=" + brediusApp.offset);

            // Set url
            //  window.location.href = window.location.pathname + "?q=" + encodeURIComponent(q);
            window.history.pushState({}, document.title, window.location.pathname + "?q=" + encodeURIComponent(q));

            $q.autocomplete("close");
            $.ajax({
                url: brediusApp.endpoint + q + "?limit=" + brediusApp.limit + "&offset=" + brediusApp.offset
            }).then(function (data) {
                brediusApp.searchResults(data);
            })
            ;
        },

        createAutocompleteResultList: function (o) {
            var q = $("#q").val(),
                re = new RegExp(q, "i"),
                label;

            if (o.titles && o.titles.title && o.titles.title.match(re) !== null) {
                label = o.titles.title;
            } else if (o.recNumber && o.recNumber.constructor === String && o.recNumber.match(re) !== null) {
                label = o.recNumber;
            } else if (o.publisher && o.publisher.match(re) !== null) {
                label = o.publisher;
            } else if (o.custom1 && o.custom1.match(re) !== null) {
                label = o.custom1;
            } else if (o.pubLocation && o.pubLocation.match(re) !== null) {
                label = o.pubLocation;
            } else if (o.contributors && o.contributors.authors && o.contributors.authors.length > 0 && o.contributors.authors.join('; ').match(re) !== null) {
                label = o.contributors.authors.join('; ');
            } else if (o.contributors && o.contributors.secondairyAuthors && o.contributors.secondairyAuthors.length > 0 && o.contributors.secondairyAuthors.join('; ').match(re) !== null) {
                label = o.contributors.secondairyAuthors.join('; ');
            } else if (o.contributors && o.contributors.tertiairyAuthors && o.contributors.tertiairyAuthors.length > 0 && o.contributors.tertiairyAuthors.join('; ').match(re) !== null) {
                label = o.contributors.tertiairyAuthors.join('; ');
            } else if (o.keywords && o.keywords.length > 0 && o.keywords.join('; ').match(re) !== null) {
                label = o.keywords.join('; ');
            }

            return {label: (label) + ' (' + o.recNumber + ')', value: o._id};
        },

        createAutocompletePageNavigator: function (data) {

            var limit = data.meta.limit,
                pages = Math.ceil(data.meta.count / limit),
                i;

            var htmlStr = "";
            htmlStr += "<table id='pages'><tr>";
            htmlStr += "<td>";
            htmlStr += "Found: " + data.meta.count;
            htmlStr += "</td>";
            htmlStr += "<td>&nbsp;</td>";
            htmlStr += "<td>";
            htmlStr += "<nav aria-label='Page navigation'>";
            htmlStr += "<ul class='pagination'>";
            htmlStr += "<li>";
            htmlStr += "<a href='#' aria-label='Previous'>";
            htmlStr += "&laquo;";
            htmlStr += "</a>";
            htmlStr += "</li>";
            for (i = 1; i <= pages; i++) {
                if (data.meta.offset === i - 1) {
                    htmlStr += "<li class='active'><a href='#'>" + i + "</a></li>";
                } else {
                    htmlStr += "<li><a href='#'>" + i + "</a></li>";
                }
            }
            htmlStr += "<li>";
            htmlStr += "<a href='#' aria-label='Next'>";
            htmlStr += "&raquo;";
            htmlStr += "</a>";
            htmlStr += "</li>";
            htmlStr += "</ul>";
            htmlStr += "</nav>";
            htmlStr += "</td>";
            htmlStr += "</tr></table>";

            return htmlStr;
        },

        getImage: function (pdfs) {
            var pdf = pdfs[0], parts = pdf.split('/'),
                filename = parts[parts.length - 1].replace(".pdf", ""),
                imageUrl = "/images/icons/small/" + filename + ".png";

            return "<img src='" + imageUrl + "' 'alt='" + pdf + "'>";
        },

        makeDisplayItem: function (book) {
            var htmlStr = "", i;
            htmlStr += "<tbody class='item' id='" + book._id + "'>\n";

            if (book.periodName !== undefined && book.periodName) {
                htmlStr += "<tr><th>periodName</th><td>" + book.periodName + "</td></tr>\n";
            }
            if (book.title !== undefined && book.title) {
                htmlStr += "<tr><th>title</th><td>" + book.title + "</td></tr>\n";
            }
            if (book.denom !== undefined && book.denom) {
                htmlStr += "<tr><th>denom</th><td>" + book.denom + "</td></tr>\n";
            }
            if (book.material !== undefined && book.material) {
                htmlStr += "<tr><th>material</th><td>" + book.material + "</td></tr>\n";
            }
            if (book.ruler !== undefined && book.ruler) {
                htmlStr += "<tr><th>ruler</th><td>" + book.ruler + "</td></tr>\n";
            }
            if (book.mint !== undefined && book.mint) {
                htmlStr += "<tr><th>mint</th><td>" + book.mint + "</td></tr>\n";
            }
            if (book.catalog !== undefined && book.catalog) {
                htmlStr += "<tr><th>catalog</th><td>" + book.catalog + "</td></tr>\n";
            }
            if (book.obverse !== undefined && book.obverse) {
                htmlStr += "<tr><th>obverse </th><td>" + book.obverse + "</td></tr>\n";
            }
            if (book.reverse !== undefined && book.reverse) {
                htmlStr += "<tr><th>reverse </th><td>" + book.reverse + "</td></tr>\n";
            }
            if (book.location !== undefined && book.location) {
                htmlStr += "<tr><th>location </th><td>" + book.location + "</td></tr>\n";
            }
            if (book.titles !== undefined && book.titles.title) {
                htmlStr += "<tr><th>title</th><td>" + book.titles.title + "</td></tr>\n";
            }
            if (book.contributors !== undefined && book.contributors.authors && book.contributors.authors.length > 0) {
                htmlStr += "<tr><th>author(s)</th><td>" + book.contributors.authors.join('; ') + "</td></tr>\n";
            }
            if (book.titles !== undefined && book.titles.tertiaryTitle) {
                htmlStr += "<tr><th>series</th><td>" + book.titles.tertiaryTitle + "</td></tr>\n";
            }
            if (book.contributors !== undefined && book.contributors.tertiaryAuthors !== undefined && book.contributors.tertiaryAuthors.length > 0) {
                htmlStr += "<tr><th>series editor(s)</th><td>" + book.contributors.tertiaryAuthors.join('; ') + "</td></tr>\n";
            }
            if (book.dates !== undefined && book.dates.length > 0) {
                htmlStr += "<tr><th>pub. date</th><td>";
                for (i = 0; i < book.dates.length; i++) {
                    htmlStr += book.dates[i].year + "<br>";
                }
                htmlStr += "</td></tr>\n";
            }
            if (book.keywords !== undefined) {
                htmlStr += "<tr><th>notes</th><td>" + book.keywords.join('; ') + "</td></tr>\n";
            }
            if (book.volume !== undefined) {
                htmlStr += "<tr><th>edition</th><td>" + book.volume + "</td></tr>\n";
            }
            if (book.pages !== undefined && (book.pages.start !== '' || book.pages.end !== '')) {
                htmlStr += "<tr><th>pages</th><td>";
                if (book.pages.start) {
                    htmlStr += book.pages.start;
                }
                if (book.pages.end) {
                    htmlStr += book.pages.end;
                }
                htmlStr += "</td></tr>\n";
            }
            if (book.pubLocation !== undefined) {
                htmlStr += "<tr><th>location</th><td>" + book.pubLocation + "</td></tr>\n";
            }
            if (book.contributors !== undefined && book.contributors.secondaryAuthors !== undefined && book.contributors.secondaryAuthors.length > 0) {
                htmlStr += "<tr><th>editor(s)</th><td>" + book.contributors.secondaryAuthors.join('; ') + "</td></tr>\n";
            }
            if (book.publisher !== undefined) {
                htmlStr += "<tr><th>publisher</th><td>" + book.publisher + "</td></tr>\n";
            }
            if (book.isbn !== undefined) {
                htmlStr += "<tr><th>isbn/issn</th><td>" + book.isbn + "</td></tr>\n";
            }
            if (book.custom1 !== undefined && book.custom1 !== '') {
                htmlStr += "<tr><th>custom1</th><td>" + book.custom1 + "</td></tr>\n";
            }
            if (book.custom2 !== undefined && book.custom2 !== '') {
                htmlStr += "<tr><th>custom2</th><td>" + book.custom2 + "</td></tr>\n";
            }
            if (book.custom3 !== undefined && book.custom3 !== '') {
                htmlStr += "<tr><th>custom3</th><td>" + book.custom3 + "</td></tr>\n";
            }
            if (book.numVols !== undefined) {
                htmlStr += "<tr><th>numVols</th><td>" + book.numVols + "</td></tr>\n";
            }
            if (book.misc1 !== undefined) {
                htmlStr += "<tr><th>misc1</th><td>" + book.misc1 + "</td></tr>\n";
            }
            if (book.urls !== undefined && book.urls['image-urls'] !== undefined && book.urls['image-urls'].length > 0) {
                htmlStr += "<tr><th>image</th><td>" + brediusApp.getImage(book.urls['image-urls']) + "</td></tr>\n";
            }
            if (book.urls !== undefined && book.urls['pdf-urls'] !== undefined && book.urls['pdf-urls'].length > 0) {
                htmlStr += "<tr><th>image</th><td>" + brediusApp.getImage(book.urls['pdf-urls']) + "</td></tr>\n";
            }
            if (book.urls !== undefined && book.urls['related-urls'] !== undefined && book.urls['related-urls'].length > 0) {
                htmlStr += "<tr><th>image</th><td>" + brediusApp.getImage(book.urls['related-urls']) + "</td></tr>\n";
            }
            if (book.picture1 !== undefined) {
                var imgUrl = book.picture1.replace('C:\\BEWERKTEFOTOSMAPVICTORIA', '').toLowerCase().split('\\');
                if (imgUrl.length > 1) {
                    imgUrl = "/images/coins/small/" + imgUrl[0] + "/" + imgUrl[1];
                    htmlStr += "<tr><th>image</th><td><img src='" + imgUrl + "' alt='" + imgUrl + "'></td></tr>\n";
                }
            }


            if (book.recNumber !== undefined) {
                htmlStr += "<tr><th>number</th><td>" + book.recNumber + "</td></tr>\n";
            }
            htmlStr += "<tr><td colspan=2'>&nbsp;</td></tr>\n";
            htmlStr += "</tbody'>\n";

            return htmlStr;
        },

        searchResults: function (data) {
            var $searchResult = $("#searchResults"),
                $searchFooter;

            brediusApp.searchResult = data;

            $searchResult.html("<tbody></tbody><tfoot></tfoot>");
            $searchFooter = $searchResult.find('tfoot');
            var htmlStr, navStr;

            $("body").css("cursor", "default");

            $searchFooter.html('');

            htmlStr = data.doc.map(brediusApp.makeDisplayItem);
            navStr = brediusApp.createAutocompletePageNavigator(data);

            $searchResult.append(htmlStr)
                .promise()
                .done(function () {

                    if (brediusApp.isAutocompleteSearch) {
                        $("#q").val('');
                    }

                });

            $searchFooter.html($('<td></td>', {
                colspan: 2,
                html: navStr,
                class: "text-right text-muted"
            }));

            $(".pagination li")
                .promise()
                .done(function (nodes) {
                    for (var i = 0; i < nodes.length; i++) {
                        if (nodes[i].className !== 'active') {
                            nodes[i].addEventListener("click", brediusApp.nextPage);
                        }
                    }
                });

        },


        nextPage: function (e) {
            var content = e.srcElement.innerHTML.replace(/<[^>]*>/g),
                pages = Math.ceil(brediusApp.searchResult.meta.count / brediusApp.searchResult.meta.limit);

            if (content === '«') {
                brediusApp.offset--;
                if (brediusApp.offset < 0) {
                    brediusApp.offset = 0;
                }
            } else if (content === '»') {
                brediusApp.offset++;
                if (brediusApp.offset >= pages) {
                    brediusApp.offset = pages - 1;
                }
            } else if (parseInt(content, 10) >= 0) {
                brediusApp.offset = content - 1;
            }

            brediusApp.search();


        },

        getURLParameter: function (s) {
            var pageUrl = window.location.search.substring(1);
            var urlVars = pageUrl.split('&');
            var i, r;
            for (i = 0; i < urlVars.length; i++) {
                var paramName = urlVars[i].split('=');
                if (paramName[0] === s) {
                    r = paramName[1];
                }
            }
            return r;
        },

        init: function () {

            brediusApp.endpoint += brediusApp.page + "/search/";

            if (brediusApp.page === 'icon-at-large') {
                var image = brediusApp.getURLParameter("i") + ".png",
                    url = "/images/icons/large/" + image,
                    imageUrl = "<img src='" + url + "' alt=''>";
                $("#largeImage").html(imageUrl);
            }

            var $q = $("#q");

            $("#searchBtn").click(function () {
                brediusApp.search();
            });

            // get query from url
            var q = brediusApp.getURLParameter('q');

            // set query field with url parameter
            $q.val(q);

            // set focus to query field
            $q.ready(function () {
                $q.focus();
            });


            $q.keyup(function (e) {
                if (e.keyCode == 13) {
                    brediusApp.search();
                }
            });

            $q.autocomplete({
                source: function (req, res) {
                    $.ajax({
                        type: "GET",
                        url: brediusApp.endpoint + $q.val() + "?limit=10&autocomplete=1",
                        dataType: "json",
                        success: function (result) {
                            res(result.doc.map(brediusApp.createAutocompleteResultList));
                        }
                    })
                },
                select: function (event, ui) {
                    $q.val(ui.item.value)
                        .promise()
                        .done(brediusApp.search());
                },
                minLength: 1
            });


            // Start search, if a term is given in the url
            if (q !== undefined && q !== '') {
                brediusApp.search();
            }
        }

    };
}());

window.onload = brediusApp.init;