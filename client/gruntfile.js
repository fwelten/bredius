/**
 * @see http://stackoverflow.com/questions/21148042/concat-bower-components-with-grunt
 */
module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({


        pkg: grunt.file.readJSON('package.json'),

        concat: {
            js: {
                src: [
                    "./bower_components/jquery/dist/jquery.js",
                    "./bower_components/jquery-ui/jquery-ui.js",
                    "./bower_components/bootstrap/dist/js/bootstrap.js",
                    "./app/javascripts/app.js",
                    "./app/javascripts/ie10-viewport-bug-workaround.js"
                ],
                dest: "./dist/js/lib.js"
            }
        },

        uglify: {
            dist: {
                files: {
                    "./dist/js/lib.min.js": ["./dist/js/lib.js"]
                }
            }
        },

        sass: {
            dist: {
                files: {
                    "./dist/css/style.css": ["./app/stylesheets/style.sass"]
                }
            }
        },

        copy: {
            dist: {
                files: [
                    {src: './app/favicon.ico', dest: './dist/favicon.ico'},
                    {src: './app/index.html', dest: './dist/index.html'},
                    {src: './app/books.html', dest: './dist/books.html'},
                    {src: './app/icons.html', dest: './dist/icons.html'},
                    {src: './app/icon-at-large.html', dest: './dist/icon-at-large.html'},
                    {src: './app/coins.html', dest: './dist/coins.html'},
                    {expand: true, cwd: "./bower_components/bootstrap-sass/assets", src: ["fonts/**"], dest: "./dist/"}
                ]
            }
        },


        cssmin: {
            dist: {
                target: {
                    files: [{
                        expand: true,
                        cwd: 'app/stylesheets',
                        src: ['*.css', '!*.min.css'],
                        dest: 'dist/css',
                        ext: '.min.css'
                    }]
                }
            }
        },

        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: {
                    './dist/index.html': './dist/index.html',
                    './dist/books.html': './dist/books.html',
                    './dist/icons.html': './dist/icons.html'
                }
            }
        },


        watch: {
            html: {
                files: ['app/*.html'],
                tasks: ['htmlmin']
            },
            js: {
                files: ['app/javascripts/**/*.js'],
                tasks: ['concat', 'uglify']
            },
            hbs: {
                files: ['app/templates/**/*.hbs'],
                tasks: ['handlebars', 'concat', 'uglify']
            },
            css: {
                files: ['app/stylesheets/**/*.css'],
                tasks: ['cssmin', 'concat', 'copy']
            },
            sass: {
                files: ['app/stylesheets/**/*.sass'],
                tasks: ['sass']
            }

        }

    });


    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    // grunt.loadNpmTasks('grunt-contrib-handlebars');

    grunt.registerTask('default', ['sass', /*'handlebars', */'cssmin', 'concat', 'uglify', 'copy', 'htmlmin', 'watch']);
};