/* jslint node: true */
"use strict";

/** @module Routes for icons */
/** @class */

var express = require('express'),
    router = express.Router();

/**  icon routes
 ---------------
 We create a variable "user" that holds the controller object.
 We map the URL to a method in the created variable "controller".
 In this example is a mapping for every CRUD action.
 */
var controller = require('../app/controllers/icons.js');

// C in CRUD - Create
router
    // Create a new icon
    .post('/icons', controller.create);

// R in CRUD - Retrieve
router
    // Retrieve all icons
    .get('/icons', controller.list)
    // Retrieve a single icon
    .get('/icons/:_id', controller.detail)
    .get('/icons/search/:q', controller.search);

// U in CRUD - Update
router
    // Update single icon
    .put('/icons/:_id', controller.updateOne);

// D in CRUD - Delete
router
    // Delete single icon
    .delete('/icons/:_id', controller.deleteOne);

module.exports = router;
