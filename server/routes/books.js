/* jslint node: true */
"use strict";

/** @module Routes for books */
/** @class */

var express = require('express'),
    router = express.Router();

/**  book routes
 ---------------
 We create a variable "user" that holds the controller object.
 We map the URL to a method in the created variable "controller".
 In this example is a mapping for every CRUD action.
 */
var controller = require('../app/controllers/books.js');

// C in CRUD - Create
router
    // Create a new book
    .post('/books', controller.create);

// R in CRUD - Retrieve
router
    // Retrieve all books
    .get('/books', controller.list)
    // Retrieve a single book
    .get('/books/:_id', controller.detail)
    .get('/books/search/:q', controller.search);

// U in CRUD - Update
router
    // Update single book
    .put('/books/:_id', controller.updateOne);

// D in CRUD - Delete
router
    // Delete single book
    .delete('/books/:_id', controller.deleteOne);

module.exports = router;
