/* jslint node: true */
"use strict";

/** @module Routes for coins */
/** @class */

var express = require('express'),
    router = express.Router();

/**  coin routes
 ---------------
 We create a variable "user" that holds the controller object.
 We map the URL to a method in the created variable "controller".
 In this example is a mapping for every CRUD action.
 */
var controller = require('../app/controllers/coins.js');

// C in CRUD - Create
router
    // Create a new coin
    .post('/coins', controller.create);

// R in CRUD - Retrieve
router
    // Retrieve all coins
    .get('/coins', controller.list)
    // Retrieve a single coin
    .get('/coins/:_id', controller.detail)
    .get('/coins/search/:q', controller.search);

// U in CRUD - Update
router
    // Update single coin
    .put('/coins/:_id', controller.updateOne);

// D in CRUD - Delete
router
    // Delete single coin
    .delete('/coins/:_id', controller.deleteOne);

module.exports = router;
