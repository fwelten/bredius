/* jslint node: true */

(function () {
    "use strict";

    /**
     * Module dependencies.
     */
    var mongoose = require('mongoose'), // Mongoose
        Schema = mongoose.Schema, // Schema class
        coinSchema, // Schema definition for the Coins model
        modelName; // Name of the model as it is known to Mongoose

    /**
     * Creates a new mongoose schema.
     * -----
     * Instructions, hints and questions
     * - Instruction: create a Schema with the following properties
     *   - title
     *   - author
     *   - description
     *   - modificationDate
     * - The properties are defined as follow:
     *   - title, author and description are strings
     *   - modificationDate is a date
     *   - a title of the coin is unique
     *   - title and author are required
     *   - the default value for date is now
     *
     * - Question: What are the differences between a 'Schema Type' and a JSON object? Use the references to motivate your answer.
     * - Question: What is the function of the 'collection' property?
     * - Question: Suppose you have a model for 'Person'. What would be the default collection name?
     * @class Schema/Coin
     * @returns Schema object
     * @see http://www.json.org/
     * @see http://mongoosejs.com/docs/schematypes.html
     * @see http://mongoosejs.com/docs/guide.html#collection
     */
    coinSchema = new Schema({
        "recNumber": {type: Number, required: true},
        "filter": {type: String},
        "periodName": {type: String},
        "title": {type: String},
        "denom": {type: String},
        "material": {type: String},
        "ruler": {type: String},
        "region": {type: String},
        "date": {type: String},
        "mint": {type: String},
        "catalog": {type: String},
        "condition": {type: String},
        "obverse": {type: String},
        "reverse": {type: String},
        "weight": {type: String},
        "diameter": {type: String},
        "mintMarks": {type: String},
        "dieAxis": {type: String},
        "type": {type: String},
        "place": {type: String},
        "status": {type: String},
        "entryDate": {type: String},
        "custom1": {type: String},
        "custom2": {type: String},
        "custom3": {type: String},
        "custom4": {type: String},
        "custom5": {type: String},
        "custom6": {type: String},
        "purchased": {type: String},
        "purchFrom": {type: String},
        "quantity": {type: String},
        "cost": {type: String},
        "evaluated": {type: String},
        "value": {type: String},
        "sold": {type: String},
        "sellPrice": {type: String},
        "purchSell": {type: String},
        "valueNote": {type: String},
        "description": {type: String},
        "notes": {type: String},
        "creationDate": {type: Date, default: Date.now},
        "modificationDate": {type: Date, default: Date.now},
        "createdBy": {type: String, default: "admin"},
        "modifiedBy": {type: String, default: "admin"}
    }, {collection: 'coins'});


    modelName = "Coin";
    mongoose.model(modelName, coinSchema);
}());
