/* jslint node: true */
"use strict";

var mongoose = require('mongoose'),
    Coin = mongoose.model('Coin'); // Coin model

/**
 * CREATE a coin
 * --------------------
 * Controller to create a coin.
 *
 * Instructions, hints and questions
 * - Read about the 'save' method from Mongoose.
 * - Use the 'save' method from Mongoose.
 *   - Question: What are the differences between MongoDb and Mongoose?
 *   - Question: explain the concepts of 'schema type' and 'model'. How are they related?
 * - Return all fields.
 * - Use the model "Coin".
 *
 * The return object has three properties:
 *
 * - meta: These are all optional and free to extend
 *   - method name: The name of the method
 *   - timestamp
 *   - filename: The name of the file. Use '__filename' for this.
 *   - duration: Duration of execution, time spend on server or other meaningful metric
 * - doc: The result object, in case of retrieving all objects, this is always an array. No documents is returned as an empty array.
 * - err: If no errors, it has the value 'null'
 *
 * Errors are not thrown in the node application but returned to the user.
 * - Question: What will happen if you throw an error on the server?
 * @param req
 * @param res
 * @see http://docs.mongodb.org/manual/reference/method/db.collection.save/
 * @see http://mongoosejs.com/docs/api.html#model_Model-save
 * @module books/create
 */
exports.create = function (req, res) {
    var doc = new Coin(req.body);
    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", 'timestamp': new Date(), filename: __filename},
            doc: doc, // Coin that was created
            err: err // Eventual error
        };
        return res.send(retObj); // Send back to the browser
    });
};

/**
 * RETRIEVE _all_ books
 * --------------------
 * Controller to retrieve _all_ books.
 *
 * Instructions, hints and questions
 * - Read about the 'find' method from Mongoose.
 * - Use the 'find' method from Mongoose.
 *   - Question: What are the differences between MongoDb and Mongoose?
 * - The 'query' parameter is an empty object.
 *   - Question: Why is it empty?
 * - Skip the options.
 *   - Question: Describe the options.
 * - Return all fields.
 * - Use the model "Coin".
 *
 * The return object has three properties:
 *
 * - meta: These are all optional and free to extend
 *   - method name: The name of the method
 *   - timestamp
 *   - filename: The name of the file. Use '__filename' for this.
 *   - duration: Duration of execution, time spend on server or other meaningful metric
 * - doc: The result object, in case of retrieving all objects, this is always an array. No documents is returned as an empty array.
 * - err: If no errors, it has the value 'null'
 *
 * Errors are not thrown in the node application but returned to the user.
 * - Question: What will happen if you throw an error on the server?
 * @param req
 * @param res
 * @see http://docs.mongodb.org/manual/reference/method/db.collection.find/
 * @see http://mongoosejs.com/docs/api.html#model_Model.find
 * @module books/list
 */
exports.list = function (req, res) {
    var conditions = {}, // No condition, find all
        fields = {}, // Return all fields
        sort = {'modificationDate': -1}; // Order by modificationdate

    Coin
        .find(conditions, fields)
        .sort(sort)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "list", 'timestamp': new Date(), filename: __filename},
                doc: doc, // Array of all books (can be empty)
                err: err // Eventual error
            };

            return res.send(retObj);
        });
};

/**
 * RETRIEVE _one_ coin
 * --------------------
 * Controller to retrieve _one_ books.
 *
 * Instructions, hints and questions
 * - Read about the 'findOne' method from Mongoose.
 * - Use the 'findOne' method from Mongoose.
 *   - Question: What is de result object from findOne?
 *   - Question: What are the differences between MongoDb and Mongoose?
 * - The 'query' parameter is an empty object.
 *   - Question: Why is it empty?
 * - Skip the options.
 * - Return all fields.
 * - Use the model "Coin".
 * Question: Define route parameters and body parameter. What are the differences?
 *
 * The return object has three properties:
 *
 * - meta: These are all optional and free to extend
 *   - method name: The name of the method
 *   - timestamp
 *   - filename: The name of the file. Use '__filename' for this.
 *   - duration: Duration of execution, time spend on server or other meaningful metric
 * - doc: The result object is either an object or null.
 * - err: If no errors, it has the value 'null'
 *
 * @module books/detail
 * @param req
 * @param res
 * @see http://docs.mongodb.org/manual/reference/method/db.collection.findOne/
 * @see http://mongoosejs.com/docs/api.html#model_Model.findOne
 */
exports.detail = function (req, res) {
    var conditions = {_id: req.params._id}, // Find coin with ID given in request
        fields = {}; // Return all fields

    Coin
        .findOne(conditions, fields)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                doc: doc, // The single found coin
                err: err // Eventual error
            };

            return res.send(retObj);
        });
};
exports.search = function (req, res) {
    var re = new RegExp(req.params.q, 'i'),
        offset = parseInt(req.query.offset, 10) || 0,
        limit = parseInt(req.query.limit, 10),
        autocomplete = req.query.autocomplete || false,
        actualLimit = autocomplete ? limit : 0,
        returnedDocs = null,
        validId = false,
        startTime = new Date(),
        conditions = {
            '$or': [
                {'f0': re},
                {'periodName': re},
                {'title': re},
                {'denom': re},
                {'material': re},
                {'ruler': re},
                {'mint': re},
                {'location': re},
                {'obverse': re},
                {'reverse': re},
                {'notes': re}
            ]
        },
        fields = {}, // Return all fields
        sort = {'f0': 1}; // Order by modificationdate

    if (isNaN(limit)) {
        limit = 0;
    }


    if (mongoose.Types.ObjectId.isValid(req.params.q) && req.params.q.match(/:/) === null) {
        conditions = {_id: req.params.q};
        validId = true;
    } else if ( req.params.q.match(/\:/) !== null) {
        var keyword = req.params.q.split(':')[0].replace(/^\s*/g, '').replace('/\s*$/g', '');
        var q = req.params.q.split(':')[1].replace(/^\s*/g, '').replace('/\s*$/g', '');
        re = new RegExp(q, 'i');

        if (keyword === 'title') {
            conditions = {'titles.title': re};
        } else if (keyword === 'author') {
            conditions = {
                '$or': [
                    {'contributors.authors': re},
                    {'contributors.secondairyAuthors': re},
                    {'contributors.tertiairyAuthors': re}
                ]
            }
        } else if (keyword === 'notes') {
            conditions = {'keywords': re}
        } else if (keyword === 'recNumber') {
            conditions = {'recNumber': re}
        } else if (keyword === 'publisher') {
            conditions = {'publisher': re}
        } else if (keyword === 'custom1') {
            conditions = {'custom1': re}
        } else if (keyword === 'location') {
            conditions = {'pubLocation': re}
        } else {
            conditions = {};
            conditions[keyword] = re;
        }
    }

    Coin
        .find(conditions, fields)
        .limit(actualLimit)
        .sort(sort)
        .exec(function (err, doc) {

            if (doc && (doc.length <= limit || doc.length === 1)) {
                returnedDocs = doc;
            } else if (limit === 0) {
                returnedDocs = doc;
            } else {
                returnedDocs = doc.slice(offset * limit, offset * limit + limit);
            }
            var retObj = {
                meta: {
                    "action": "list",
                    'timestamp': new Date(),
                    filename: __filename,
                    count: doc.length,
                    offset: offset,
                    duration: new Date() - startTime,
                    limit: limit,
                    validId: validId,
                    q: req.params.q
                },
                doc: returnedDocs, // Array of all books (can be empty)
                err: err // Eventual error
            };

            return res.send(retObj);
        });
};

/**
 * UPDATE coin
 * --------------------
 * Controller to update _one_ books.
 *
 * Instructions, hints and questions
 * - Read about the 'find' method from Mongoose.
 * - Use the 'findOneAndUpdate' method from Mongoose.
 *   - Question: What are the differences between MongoDb and Mongoose?
 *   - Question: What are the differences between MongoDb 'save' and MongoDb 'update'?
 * - Return all fields.
 * - Use the model "Coin".
 * Question: What changes should be made to update more than one document?
 *
 * The return object has three properties:
 *
 * - meta: These are all optional and free to extend
 *   - method name: The name of the method
 *   - timestamp
 *   - filename: The name of the file. Use '__filename' for this.
 *   - duration: Duration of execution, time spend on server or other meaningful metric
 * - doc: The result object is either an object or null.
 * - err: If no errors, it has the value 'null'
 *
 * @module books/update
 * @param req
 * @param res
 * @see http://docs.mongodb.org/manual/reference/method/db.collection.update/
 * @see http://docs.mongodb.org/manual/reference/method/db.collection.save/
 * @see http://mongoosejs.com/docs/api.html#model_Model.findOneAndUpdate
 */
exports.updateOne = function (req, res) {
    var conditions = {_id: req.params._id},
        update = { // Create object with updated values
            title: req.body.doc.title || '',
            author: req.body.doc.author || '',
            description: req.body.doc.description || ''
        },
        options = {multi: false}, // Single document updated
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "update", 'timestamp': new Date(), filename: __filename},
                doc: doc, // Updated document
                err: err // Eventual error
            };

            return res.send(retObj);
        };

    Coin
        .findOneAndUpdate(conditions, update, options, callback);
};

/**
 * DELETE
 * remove @ http://mongoosejs.com/docs/api.html#model_Model-remove
 * @param req
 * @param res
 */

/**
 * DELETE _one_ coin
 * --------------------
 * Controller to delete _one_ books.
 *
 * Instructions, hints and questions
 * - Read about the 'findOne' method from Mongoose.
 * - Use the 'findOne' method from Mongoose.
 *   - Question: What is de result object from findOne?
 *   - Question: What are the differences between MongoDb and Mongoose?
 * - The 'query' parameter is an empty object.
 *   - Question: Why is it empty?
 * - Skip the options.
 * - Return all fields.
 * - Use the model "Coin".
 * Question: Define route parameters and body parameter. What are the differences?
 *
 * The return object has three properties:
 *
 * - meta: These are all optional and free to extend
 *   - method name: The name of the method
 *   - timestamp
 *   - filename: The name of the file. Use '__filename' for this.
 *   - duration: Duration of execution, time spend on server or other meaningful metric
 * - doc: The result object is either an object or null.
 * - err: If no errors, it has the value 'null'
 *
 * @module books/detail
 * @param req
 * @param res
 * @see http://docs.mongodb.org/manual/reference/method/db.collection.remove/
 * @see http://mongoosejs.com/docs/api.html#model_Model.remove
 */
exports.deleteOne = function (req, res) {
    var conditions = {_id: req.params._id},
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "delete", 'timestamp': new Date(), filename: __filename},
                doc: doc, // Deleted document
                err: err // Eventual error
            };

            return res.send(retObj);
        };

    Coin
        .remove(conditions, callback);
};
