/* jslint node: true */

(function () {
    "use strict";

    /** TODO: Test with static-analyzer: define module */

    /**
     * Module dependencies.
     * @type {exports}
     */
    var fs = require('fs'), // Used to read files from the filesystem (__dirname).
        //http = require('http'), // Enables HTTP protocol.
        express = require('express'), // Fast, unopinionated minimalist web framework for Node.js.
        bodyParser = require('body-parser'), // This does not handle multipart bodies due to their complex and typically large nature.
        env, // Store app profile (development/test/production/etc...)
        config, // Store config
        mongoose, // Database driver
        models_path, // Path to the 'app/models' directory
        model_files, // File names in the 'app/models' directory
        app, // Express app
        routes_path, // Path to the 'routes' directory
        route_files; // File names in the 'routes' directory

    /**
     * Load configuration
     * @type {*|string}
     */
    env = process.env.NODE_ENV || 'development'; // You can use the environment-variable 'NODE_ENV' to determine the environment being used. Fall back to 'development' when this is unset.
    console.log("Running profile: " + env);
    config = require('./config/config.js')[env]; // Load configuration file
    console.log("Database: " + config.db + ", port: " + (process.env.PORT || config.port) + ", debugging: " + (config.debug ? "enabled" : "disabled"));

    /**
     * Bootstrap db connection
     * @type {exports}
     */
    mongoose = require('mongoose');
    mongoose.connect(config.db); // Connect to database, use settings defined in 'config/config.js'

    /**
     * Configure debugging for mongoose
     */
    mongoose.connection.on('error', function (err) { // Log to the console when a Mongo connection error occurs
        console.error("MongoDB error: %s", err);
    });
    mongoose.set('debug', config.debug);

    /**
     * Bootstrap models
     * @type {string}
     */
    models_path = __dirname + '/app/models';
    model_files = fs.readdirSync(models_path); // List of all files in the 'app/models' directory
    model_files.forEach(function (file) { // Load all models
        require(models_path + '/' + file);
    });

    /**
     * Use express
     * @type {*}
     */
    app = express();

    /**
     * Express settings
     */
    app.set('port', process.env.PORT || config.port); // Set the port, which can be configured using the environment variable 'PORT', or via 'config/config.js'


    /**
     *
     * CORS middleware
     */
    var allowCrossDomain = function(req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.header('Access-Control-Allow-Headers', 'Content-Type');

        next();
    };
    app.use(allowCrossDomain); // Incoming requests will be in JSON format

    /**
     * Express middleware
     */
    app.use(bodyParser.json()); // Incoming requests will be in JSON format
    app.use(bodyParser.urlencoded({extended: true})); // Encoding for bodies

    /**
     * Middleware to enable logging
     */
    if (config.debug) {
        app.use(function (req, res, next) { // Request logging
            console.log('%s %s %s', req.method, req.url, req.path);
            next(); // Required to move on
        });
    }


    ;
    /**
     * Middleware to serve static page
     */
    app.use(express.static(__dirname + '/../client/dist/')); // Serve Angular-app from '../client' directory

    /**
     * Bootstrap routes
     * @type {string}
     */
    routes_path = __dirname + '/routes';
    route_files = fs.readdirSync(routes_path);
    route_files.forEach(function (file) { // Load all routes defined in the 'routes' directory
        var route = require(routes_path + '/' + file);
        app.use('/api', route);
    });

    /**
     * Middleware to catch all unmatched routes
     */
    app.all('*', function (req, res) {
        res.sendStatus(404);
    });

    module.exports = app; // Expose Express app for other modules/files to use.
}());
